import React, { useEffect } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import { fcmService } from './src/component/Notification/FCMService';
// import configureStore from './redux/configureStore';
import Navigation from './src/navigation/Navigation';
import configureStore from './src/redux/configureStore';
const {store, persistor} = configureStore();

const App = () => {
    const initNotification =()=> {

        function onRegister(fcmToken) {
            console.log('[App] onRegister: ', fcmToken);
          }

        fcmService.registerAppWithFCM();
        fcmService.register(onRegister);
      }

    useEffect(()=>{
        initNotification()
    })



      return (
        <View style={{ flex: 1 }}>
            <Provider store={store}>
            <PersistGate persistor={persistor}>
                <Navigation />
                </PersistGate>
            </Provider>
        </View>
    )
}

export default App;