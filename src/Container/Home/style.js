import { StyleSheet } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    wholeView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonView: {
        width: '30%',
        borderRadius:wp('2%'),
        backgroundColor: '#1050e6',
        justifyContent: 'center',
        alignItems: 'center',

    },
    itemsInRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-around',
        width:'100%'
    },
    valueView: {
        fontSize: wp('11.5%'),
    },
    shadow: {
        shadowColor: "green",
        shadowOpacity:1,
        shadowOffset: { width: 1, height: 1 },
        shadowRadius: 8.30,
        elevation: 10,
        zIndex: 5
    }
})

export { styles };