import React, { Component } from 'react';
import { View, Text,Platform, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hitUserRequest, setDecrementCounter, setIncrementCounter, setName } from '../../Modules/Home/actions';
import { styles } from './style';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import  PushNotification from "react-native-push-notification";
import { fcmService } from '../../component/Notification/FCMService';
import { localNotificationService } from '../../component/Notification/LocalNotificationService';
import Sound from 'react-native-sound';

const sound = new Sound(require('../../assets/sound.mp3'))

class HomeScreen extends Component {

    componentDidMount(){
        this.props.hitUserRequest()
        this.initNotification()
    }

    initNotification() {
        if (Platform.OS == 'android') {
          PushNotification.createChannel(
            {
              channelId: 'channel-id', // (required)
              channelName: 'My channel', // (required)
              channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
              playSound: false, // (optional) default: true
              soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
              importance: 4, // (optional) default: Importance.HIGH. Int value of the Android notification importance
              vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
            },
            (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
          );
        }
    
        fcmService.registerAppWithFCM();
        fcmService.notificationListeners(
          onRegister,
          onNotification,
          onOpenNotification,
        );
        localNotificationService.configure(onOpenNotification);
    
        function onRegister(token) {
          console.log('[App] MapSearch: ', token);
        }
    
        function onNotification(notify) {
          console.log('[App] onNotification: ', notify);
          const options = {
            soundName: 'default',
            playSound: true,
          };
          localNotificationService.showNotification(
            0,
            notify.title,
            notify.body,
            notify,
            options,
          );
        }
    
        function onOpenNotification(notify) {
            sound.play()
          console.log('[App] onOpenNotification: ', notify);
          //  alert('Open Notification: ' + notify.body);
        }
      }

    render() {
        console.log(this.props.users);
        return (
            <View style={styles.wholeView}>
                <Text style={styles.valueView}>{this.props.count}</Text>

                <View style={styles.itemsInRow}>

                    <TouchableOpacity
                        style={[styles.buttonView, styles.shadow]}
                        onPress={() => {
                            this.props.setDecrementCounter(this.props.count)

                        }}
                    >
                        <Text style={{ fontSize: wp('10%'), fontWeight: '300', color: 'white' }}>-</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[styles.buttonView, styles.shadow]}
                        onPress={() => this.props.setIncrementCounter(this.props.count)}
                    >
                        <Text style={{ fontSize: wp('10%'), fontWeight: '300', color: 'white' }} >+</Text>
                    </TouchableOpacity>

                </View>

                <TextInput
                    placeholder='Name'
                    style={{ padding: 5, }}
                    onChangeText={(txt) => { this.props.setName(txt) }}
                />

                <TouchableOpacity
                    style={[styles.shadow, { backgroundColor: 'red' }]}
                    onPress={() => {
                        this.props.setDecrementCounter(this.props.count)

                    }}
                >
                    <Text style={{ fontSize: wp('6%'), fontWeight: '300', color: 'white' }}>Update name</Text>
                </TouchableOpacity>

<Text>{this.props.name} </Text>
            </View>
        )
    }
}


// const mapStateToProps = (state) => ({
//     // console.log('state.countReducer==',state.countReducer.count)
//     count: state.countReducer.count
// });

const mapStateToProps = (state) => ({
    count: state.countReducer.count,
    name: state.countReducer.name,
    users:state.countReducer.users,

});


const mapDispatchToProps = (dispatch) => ({
    setIncrementCounter: (data) => dispatch(setIncrementCounter(data)),
    setDecrementCounter: (data) => dispatch(setDecrementCounter(data)),
    setName: (name) => dispatch(setName(name)),
    hitUserRequest: (name) => dispatch(hitUserRequest(name)),

});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);