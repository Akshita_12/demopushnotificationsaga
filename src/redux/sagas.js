import { all } from 'redux-saga/effects';
//====================== APP SETTINGS =====================
import sagaCounter from '../Modules/Home/sagas';

export default function* rootSaga() {
  yield all([
    sagaCounter()
  ]);
}
