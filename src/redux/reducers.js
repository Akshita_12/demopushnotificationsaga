import { combineReducers } from 'redux';
//====================== APP SETTINGS =====================
import countReducer from '../Modules/Home/reducer'

const appReducer = combineReducers({
   countReducer
});

const initialState = appReducer({}, {});
const rootReducer = (state, action) => {
  state = Object.assign({}, initialState);

  return appReducer(state, action);
};

export default rootReducer;


