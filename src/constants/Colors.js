const Colors = {
  PrimaryColor: '#FF337D',
  background: '#ffffff',
  buttonGreen: '#40BA52',
  buttonRed: '#E93F18',
  placeholderColor: '#818e97',
  photoBorder: '#FC68A7',
  bottomBorder: '#c6c6c6', //'#d1d1d1'
  placeholderTextColor: '#818e97',
  White: 'white',
  Black: '#000000',
  buttonGreen: '#40BA52',
  iYellow: '#e1b300',
  iRed: '#8B0000',

  buttonsColor: '#FF337D',
  graylights: '#f7f8f9',
  graylightss: '#f7f9f0',
  graylighted: '#F0F0F0',
  graylight: '#C0C0C0',

  colorLight: '#97ADB6',
  colorDark: '#075E54',

  FontDarkColor: '#3E4958',
  FontMediumDark: '#736F6F',
  ColorSignup: '#FF337D',
};
export default Colors;
