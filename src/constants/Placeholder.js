const placeholder = {
  mobileNumber: 'Mobile Number',
  password: 'Password',
  emailAddress: 'Email Address',
  address: 'Address',
};
export default placeholder;
