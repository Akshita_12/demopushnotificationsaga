import { DECREMENT, INCREMENT, SET_COUNT, SET_NAME, SET_USER_LIST } from './types';

const INITIAL_STATE = {
  count: 0,
  name: 'Test2',
  users:[]
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case SET_USER_LIST:
      return {
        ...state,
        users: action.data
      };

    case SET_COUNT:
      return {
        ...state,
        count: action.data
      };

    case SET_NAME:
      return {
        ...state,
        name: action.data
      };

    default:
      return state;
  }
};
