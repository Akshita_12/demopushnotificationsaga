import { takeEvery, put } from 'redux-saga/effects';
import Config from '../../constants/Config';
import { get } from '../../utils/api'
var axios = require('axios');

import { setCount, setName, setUserList } from './actions';
import { DECREMENT, HIT_USER_API, INCREMENT, SET_NAME } from './types';

function* onIncrement(count) {
  try {
    count = count.data + 1;
    yield put(setCount(count));
  } catch (error) {
    console.log('error getting==', error)
    console.log(JSON.stringify(error));
  }
}

function* onDecrement(count) {
  try {
    if (count.data == 0) {
      count = 0
    } else {
      count = count.data - 1;
    }
    yield put(setCount(count));
  } catch (error) {
    console.log(JSON.stringify(error));
  }
}

function* onUserRequest() {

  try {
    var config = {
      method: 'get',
      url: Config.Placeholder,
      headers: {}
    };

   let res = axios(config).then(function (response) {
      let res =  JSON.stringify(response.data);
      // console.log('res comingg===',res)
      put(setUserList(res))

       return res
      })
      .catch(function (error) {
        console.log('error coming====',error);
      });

  } catch (error) {
    // yield* hideLoader(false, '');
    // yield put(profileFail());
  }
}

function* sagaCounter() {
  yield takeEvery(INCREMENT, onIncrement);
  yield takeEvery(DECREMENT, onDecrement);
  yield takeEvery(HIT_USER_API, onUserRequest);
}
export default sagaCounter;
