import { DECREMENT, SET_COUNT, INCREMENT, SET_NAME, SET_USER_LIST, HIT_USER_API } from "./types";

export const setIncrementCounter = (count) => ({
  type: INCREMENT,
  data: count
});

export const setDecrementCounter = (count) => ({
  type: DECREMENT,
  data: count 
});

export const setCount = (count) => ({
  type: SET_COUNT,
  data: count,
});

export const setName = (name) => ({
  type: SET_NAME,
  data: name,
});

export const hitUserRequest = () => ({
  type: HIT_USER_API,
});

export const setUserList = (list) => ({
  type: SET_USER_LIST,
  data: list,
});



